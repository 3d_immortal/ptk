# The Pegacee Toolkit (PTK)
[![pipeline status](https://gitlab.com/3d_immortal/ptk/badges/master/pipeline.svg)](https://gitlab.com/3d_immortal/ptk/commits/master)

## About
This is a C++ UI toolkit library for Linux with a Blender 3D user interface
look. It offers a modern C++ declarative API that is concise and very easy to
use. It uses [Blendish][1] on top of [NanoVG][2] to achieve the Blender 2.5+
user interface look.

[1]: https://bitbucket.org/duangle/oui-blendish/overview
[2]: https://github.com/memononen/nanovg

### Why "Pegacee"?
I love outer Space. This project was named after [**51 Pegasi**][3], which is
the first Sun-like star found to have an [exoplanet][4]. I changed the name to
**Pegacee** since it's a C++ project (`c` had to be there).

[3]: https://en.wikipedia.org/wiki/51_Pegasi
[4]: https://en.wikipedia.org/wiki/Exoplanet

## Status
Unstable - Experimental.

## Example
To build a simple UI as in the following demo:

![demo](./demo/ptk_demo.gif)

All you need to write is the following:

```cpp
#define OUT(stream) std::cout << stream << std::endl;

int main() {
  ptk::Application app{2.0f};
  if (!app.Init())
    return -1;

  // Use a fill layout.
  app.root_widget()->SetLayoutStrategy<ptk::FillLayout>(
      ptk::FillLayout::Type::kHorizontal);

  ptk::Widget left{app.root_widget()};
  left.SetLayoutStrategy<ptk::FillLayout>();
  ptk::Button b1{&left, "Test 1", BND_ICON_SAVE_PREFS};
  ptk::Button b2{&left, "Test 2", BND_ICON_REC};
  ptk::Button b3{&left, "Test 3", BND_ICON_PLAY};

  b1.SetOnMouseEventHandler([](ptk::MouseEvent* me) -> bool {
    if (me->flags_ & ptk::Event::FLAG_PRESSED) {
      OUT("Received mouse click at [" << me->x_ << ", " << me->y_ << "].");
      return true;
    }
    return false;
  });

  ptk::Widget right{app.root_widget()};
  right.SetLayoutStrategy<ptk::FillLayout>();
  ptk::Label label{&right, "Label 1", BND_ICON_FILE};
  ptk::Slider slider{&right, "Slider"};
  ptk::Checkbox check1{&right, "Easy?", true};
  ptk::Checkbox check2{&right, "Disable radios", false};
  ptk::RadioButton radio1{&right, "Radio 1", BND_ICON_SOUND, false};
  ptk::RadioButton radio2{&right, "Radio 2", BND_ICON_ACTION, true};
  check2.SetOnCheckedChangedHandler([&](bool checked) {
    radio1.set_is_enabled(!checked);
    radio2.set_is_enabled(!checked);
  });
  ptk::Textbox textbox{&right};
  ptk::Textbox textbox2{&right};

  return app.Run();
}
```

## How To Build

### Prerequisites
- The [BUCK](https://buckbuild.com/setup/getting_started.html) build system is
needed in order to build and test this library.
- `sudo apt install libfreetype6-dev libglfw3-dev libglu1-mesa-dev libglew-dev`

### Build & Run the Example
Run `make r-run`

### License
MIT