d-build:
	buck build @config/debug src/examples:example1

r-build:
	buck build @config/release src/examples:example1

d-run:
	buck run @config/debug src/examples:example1

r-run:
	buck run @config/release $(shell echo "src/examples:example1#strip-all")
	
clean:
	buck clean