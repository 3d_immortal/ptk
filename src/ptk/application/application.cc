
#include "src/ptk/application/application.h"

#include <chrono>
#include <thread>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "src/ptk/events/event.h"
#include "src/ptk/layout/rect.h"
#include "src/ptk/widgets/root_widget.h"
#include "src/third_party/blender_icons/blender_icons.h"
#include "src/third_party/fonts/deja_vu_sans_font.h"
#include "src/third_party/nanovg/nanovg/src/nanovg_gl.h"
#include "src/third_party/nanovg/nanovg/src/nanovg.h"

namespace ptk {

namespace {

Application* g_application = nullptr;

}  // namespace

Application::Application(float scale_factor) : scale_factor_(scale_factor) {
  DCHECK(!g_application);
  g_application = this;
}

Application::~Application() {
  if (glfw_window_)
    glfwTerminate();
  if (nvg_context_)
    nvgDeleteGL3(nvg_context_);
}

bool Application::Init() {
  DCHECK(!glfw_window_);

  if (!glfwInit()) {
    ERROR("Failed to init GLFW.");
    return false;
  }

  glfwSetErrorCallback(OnGlfwError);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, 1);

  const int kWidth = 650 * scale_factor_;
  const int kHeight = 650 * scale_factor_;

  glfw_window_ =
      glfwCreateWindow(kWidth, kHeight, "Ptk demo", nullptr, nullptr);
  if (!glfw_window_) {
    glfwTerminate();
    return false;
  }

  glfwSetKeyCallback(glfw_window_, OnKeyEvent);
  glfwSetCharCallback(glfw_window_, OnCharEvent);
  glfwSetMouseButtonCallback(glfw_window_, OnMouseButton);
  glfwSetScrollCallback(glfw_window_, OnScrollEvent);

  glfwMakeContextCurrent(glfw_window_);
  glewExperimental = GL_TRUE;
  if (glewInit() != GLEW_OK) {
    ERROR("Could not init glew.");
    glfwTerminate();
    glfw_window_ = nullptr;
    return false;
  }
  // GLEW generates GL error because it calls glGetString(GL_EXTENSIONS),
  // we'll consume it here.
  glGetError();

  // nvg_context_ = nvgCreateGL3(NVG_ANTIALIAS | NVG_STENCIL_STROKES);
  nvg_context_ = nvgCreateGL3(NVG_ANTIALIAS);
  if (!nvg_context_) {
    ERROR("Could not init nanovg.");
    glfwTerminate();
    glfw_window_ = nullptr;
    return -1;
  }

  root_widget_ = std::make_unique<RootWidget>(ptk::Rect{0, 0, kWidth, kHeight});

  bndSetFont(nvgCreateFontMem(nvg_context_, "system",
                              (unsigned char*)kDejaVuSansFont,
                              kDejaVuSansFontSize, 0));
  bndSetIconImage(nvgCreateImageMem(
      nvg_context_, 0, (unsigned char*)kBlenderIcons, kBlenderIconsSize));

  return true;
}

int Application::Run() {
  glfwSwapInterval(1);
  glfwSetTime(0);

  while (!glfwWindowShouldClose(glfw_window_)) {
    int win_width;
    int win_height;
    glfwGetWindowSize(glfw_window_, &win_width, &win_height);

    int frame_buffer_width;
    int frame_buffer_height;
    glfwGetFramebufferSize(glfw_window_, &frame_buffer_width,
                           &frame_buffer_height);

    // Calculate pixel ratio for hi-dpi devices.
    // const float pixel_ratio = (float)frame_buffer_width / (float)win_width;
    const float pixel_ratio = scale_factor_;

    // --> Start Update.
    root_widget_->SetBounds({0, 0, (int)(win_width / pixel_ratio),
                             (int)(win_height / pixel_ratio)});
    glfwGetCursorPos(glfw_window_, &mouse_context_.x_, &mouse_context_.y_);
    mouse_context_.x_ /= pixel_ratio;
    mouse_context_.y_ /= pixel_ratio;

    std::unique_ptr<MouseEvent> mouse_event;
    if (!mouse_context_.was_dispatched_) {
      mouse_event = std::make_unique<MouseEvent>(
          mouse_context_.x_, mouse_context_.y_, mouse_context_.button_,
          mouse_context_.type_, mouse_context_.modifiers_);
      mouse_context_.was_dispatched_ = true;
    } else {
      if (mouse_context_.type_ == GLFW_PRESS) {
        mouse_event = std::make_unique<MouseCapture>(
            mouse_context_.start_x_, mouse_context_.start_y_, mouse_context_.x_,
            mouse_context_.y_, mouse_context_.button_, mouse_context_.type_,
            mouse_context_.modifiers_);
      } else {
        // Already dispatched, no drag events.
        mouse_event = std::make_unique<MouseEvent>(
            mouse_context_.x_, mouse_context_.y_, -1, -1, -1);
      }
    }
    static Widget* last_hover_widget = nullptr;
    Widget* hover_widget =
        root_widget_->FindHoverWidget(mouse_context_.x_, mouse_context_.y_);
    if (hover_widget) {
      hover_widget->set_has_hover(true);
      hover_widget->OnMouseEvent(mouse_event.get());
    }
    if (last_hover_widget && (last_hover_widget != hover_widget))
      last_hover_widget->set_has_hover(false);
    last_hover_widget = hover_widget;
    if (Widget::focused_widget_ && Widget::focused_widget_->is_enabled()) {
      for (auto& input_event : input_events_) {
        if (input_event->GetType() == Event::Type::kKey) {
          Widget::focused_widget_->OnKeyEvent(
              static_cast<KeyEvent*>(input_event.get()));
        } else if (input_event->GetType() == Event::Type::kChar) {
          Widget::focused_widget_->OnCharEvent(
              static_cast<CharEvent*>(input_event.get()));
        }
      }
    }
    // Must clear input events event if there were no consumers.
    input_events_.clear();
    // --> End Update.

    // --> Start Draw.
    glViewport(0, 0, win_width, win_height);
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    nvgBeginFrame(nvg_context_, (int)(win_width / pixel_ratio),
                  (int)(win_height / pixel_ratio), pixel_ratio);
    root_widget_->Draw(nvg_context_);
    nvgEndFrame(nvg_context_);
    glfwSwapBuffers(glfw_window_);
    // --> End Draw.

    using namespace std::chrono_literals;
    std::this_thread::sleep_for(16ms);
    glfwWaitEvents();
  }

  return 0;
}

// static
void Application::OnGlfwError(int error, const char* desc) {
  ERROR("GLFW error [" << error << "]: " << desc);
}

// static
void Application::OnKeyEvent(GLFWwindow* window,
                             int key,
                             int scancode,
                             int type,  // Pressed. Released. Repeat.
                             int modifiers) {
  if (key == GLFW_KEY_ESCAPE && type == GLFW_PRESS)
    glfwSetWindowShouldClose(window, GL_TRUE);

  g_application->input_events_.emplace_back(
      std::make_unique<KeyEvent>(key, type, modifiers));
}

// static
void Application::OnCharEvent(GLFWwindow* window, unsigned int value) {
  g_application->input_events_.emplace_back(std::make_unique<CharEvent>(value));
}

// static
void Application::OnMouseButton(GLFWwindow* window,
                                int button,
                                int type,
                                int modifiers) {
  g_application->mouse_context_.button_ = button;
  g_application->mouse_context_.type_ = type;
  g_application->mouse_context_.modifiers_ = modifiers;
  g_application->mouse_context_.was_dispatched_ = false;
  if (type == GLFW_PRESS) {
    g_application->mouse_context_.start_x_ = g_application->mouse_context_.x_;
    g_application->mouse_context_.start_y_ = g_application->mouse_context_.y_;
  }
}

// static
void Application::OnScrollEvent(GLFWwindow* window, double x, double y) {}

}  // namespace ptk
