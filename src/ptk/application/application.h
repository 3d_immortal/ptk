
#ifndef SRC_PTK_APPLICATION_APPLICATION_H_
#define SRC_PTK_APPLICATION_APPLICATION_H_

#include <memory>
#include <vector>

#include "core/src/macros.h"

struct GLFWwindow;
struct NVGcontext;

namespace ptk {

class Event;
class RootWidget;

class Application {
 public:
  explicit Application(float scale_factor);
  ~Application();

  RootWidget* root_widget() const { return root_widget_.get(); }

  bool Init();

  int Run();

 private:
  static void OnGlfwError(int error, const char* desc);

  static void OnKeyEvent(GLFWwindow* window,
                         int key,
                         int scancode,
                         int type,  // Pressed. Released. Repeat.
                         int modifiers);

  static void OnCharEvent(GLFWwindow* window, unsigned int value);

  static void OnMouseButton(GLFWwindow* window,
                            int button,
                            int type,
                            int modifiers);

  static void OnScrollEvent(GLFWwindow* window, double x, double y);

  struct MouseContext {
    double x_{0.0};
    double y_{0.0};
    double start_x_{0.0};
    double start_y_{0.0};
    int button_{-1};
    int type_{-1};
    int modifiers_{-1};
    bool was_dispatched_{false};
  };

  float scale_factor_;
  MouseContext mouse_context_;
  GLFWwindow* glfw_window_{nullptr};
  NVGcontext* nvg_context_{nullptr};
  std::vector<std::unique_ptr<Event>> input_events_;
  std::unique_ptr<RootWidget> root_widget_{nullptr};

  DISALLOW_COPY_AND_ASSIGN(Application);
};

}  // namespace ptk

#endif  // SRC_PTK_APPLICATION_APPLICATION_H_
