
#ifndef SRC_PTK_EVENTS_EVENT_H_
#define SRC_PTK_EVENTS_EVENT_H_

#include "core/src/macros.h"

namespace ptk {

class Event {
 public:
  enum Flags {
    FLAG_NONE = 0,
    FLAG_PRESSED = 1 << 0,
    FLAG_RELEASED = 1 << 1,
    FLAG_REPEAT = 1 << 2,
    FLAG_CHAR = 1 << 3,
  };

  enum class Type {
    kNone,
    kMouse,
    kMouseCapture,
    kKey,
    kChar,
  };

  Event(int glfw_type);
  virtual ~Event() = default;

  virtual Type GetType() const;

  int flags_{0};
  bool handled_{false};

 private:
  DISALLOW_COPY_AND_ASSIGN(Event);
};

class MouseEvent : public Event {
 public:
  MouseEvent(double x, double y, int button, int glfw_type, int modifiers);
  ~MouseEvent() override = default;

  Type GetType() const override;

  double x_;
  double y_;
  int button_;

 private:
  DISALLOW_COPY_AND_ASSIGN(MouseEvent);
};

class MouseCapture : public MouseEvent {
 public:
  MouseCapture(double start_x,
               double start_y,
               double x,
               double y,
               int button,
               int glfw_type,
               int modifiers);
  ~MouseCapture() override = default;

  Type GetType() const override { return Type::kMouseCapture; }

  double start_x_;
  double start_y_;

 private:
  DISALLOW_COPY_AND_ASSIGN(MouseCapture);
};

class KeyEvent : public Event {
 public:
  KeyEvent(int key, int glfw_type, int modifiers);
  ~KeyEvent() override = default;

  Type GetType() const override;

  int key_;
};

class CharEvent : public Event {
 public:
  CharEvent(unsigned int key);
  ~CharEvent() override = default;

  Type GetType() const override;

  unsigned int key_;

 private:
  DISALLOW_COPY_AND_ASSIGN(CharEvent);
};

}  // namespace ptk

#endif  // SRC_PTK_EVENTS_EVENT_H_
