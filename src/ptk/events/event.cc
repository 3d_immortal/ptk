
#include "src/ptk/events/event.h"

#include <GLFW/glfw3.h>

namespace ptk {

Event::Event(int glfw_type) {
  switch (glfw_type) {
    case GLFW_PRESS:
      flags_ = FLAG_PRESSED;
      break;
    case GLFW_RELEASE:
      flags_ = FLAG_RELEASED;
      break;
    case GLFW_REPEAT:
      flags_ = FLAG_REPEAT;
      break;
    default:
      flags_ = FLAG_NONE;
      break;
  }
}

ptk::Event::Type Event::GetType() const {
  return Type::kNone;
}

MouseEvent::MouseEvent(double x,
                       double y,
                       int button,
                       int glfw_type,
                       int modifiers)
    : Event(glfw_type), x_(x), y_(y), button_(button) {}

ptk::Event::Type MouseEvent::GetType() const {
  return Type::kMouse;
}

MouseCapture::MouseCapture(double start_x,
                           double start_y,
                           double x,
                           double y,
                           int button,
                           int glfw_type,
                           int modifiers)
    : MouseEvent(x, y, button, glfw_type, modifiers),
      start_x_(start_x),
      start_y_(start_y) {}

KeyEvent::KeyEvent(int key, int glfw_type, int modifiers)
    : Event(glfw_type), key_(key) {}

ptk::Event::Type KeyEvent::GetType() const {
  return Type::kKey;
}

CharEvent::CharEvent(unsigned int key) : Event(-1), key_(key) {}

ptk::Event::Type CharEvent::GetType() const {
  return Type::kChar;
}

}  // namespace ptk
