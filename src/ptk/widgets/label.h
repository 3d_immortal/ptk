
#ifndef SRC_PTK_WIDGETS_LABEL_H_
#define SRC_PTK_WIDGETS_LABEL_H_

#include <string>

#include "src/ptk/widgets/widget.h"

namespace ptk {

class Label : public Widget {
 public:
  Label(Widget* parent, const char* label, int icon_id);
  ~Label() override = default;

 protected:
  // ptk::Widget:
  void DrawInternal(NVGcontext* nvg_context) override;

  std::string label_;
  int icon_id_;

 private:
  DISALLOW_COPY_AND_ASSIGN(Label);
};

}  // namespace ptk

#endif  // SRC_PTK_WIDGETS_LABEL_H_
