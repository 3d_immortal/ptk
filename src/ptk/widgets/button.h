
#ifndef SRC_PTK_WIDGETS_BUTTON_H_
#define SRC_PTK_WIDGETS_BUTTON_H_

#include <functional>

#include "src/ptk/widgets/label.h"

namespace ptk {

class Button : public Label {
 public:
  Button(Widget* parent, const char* label, int icon_id);
  ~Button() override = default;

  void SetOnMouseEventHandler(std::function<bool(MouseEvent*)>&& handler);

 private:
  // ptk::Widget:
  DrawState CalculateDrawState() const override;
  void DrawInternal(NVGcontext* nvg_context) override;
  void OnMouseEventInternal(MouseEvent* mouse_event) override;

  std::function<bool(MouseEvent*)> on_mouse_event_;

  DISALLOW_COPY_AND_ASSIGN(Button);
};

}  // namespace ptk

#endif  // SRC_PTK_WIDGETS_BUTTON_H_
