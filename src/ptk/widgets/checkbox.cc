
#include "src/ptk/widgets/checkbox.h"

#include "src/ptk/events/event.h"
#include "src/third_party/blendish/blendish.h"

namespace ptk {

Checkbox::Checkbox(Widget* parent, const char* label, bool initial_checked)
    : Widget(parent), label_(label), checked_(initial_checked) {}

void Checkbox::SetChecked(bool checked) {
  if (checked == checked_)
    return;

  checked_ = checked;
  if (on_checked_changed_)
    on_checked_changed_(checked_);
}

void Checkbox::SetOnCheckedChangedHandler(std::function<void(bool)>&& handler) {
  on_checked_changed_ = std::move(handler);
}

ptk::Widget::DrawState Checkbox::CalculateDrawState() const {
  if (checked_)
    return DrawState::ACTIVE;

  if (has_hover())
    return DrawState::HOVER;

  return DrawState::NORMAL;
}

void Checkbox::DrawInternal(NVGcontext* nvg_context) {
  bndOptionButton(nvg_context, bounds_.x_, bounds_.y_, bounds_.width_,
                  bounds_.height_, static_cast<BNDwidgetState>(draw_state_),
                  label_.c_str());
}

void Checkbox::OnMouseEventInternal(MouseEvent* mouse_event) {
  set_has_focus(mouse_event->flags_ &
                (Event::FLAG_PRESSED | Event::FLAG_REPEAT));
  if (mouse_event->GetType() != Event::Type::kMouseCapture &&
      (mouse_event->flags_ & Event::FLAG_PRESSED) &&
      !(mouse_event->flags_ & Event::FLAG_REPEAT)) {
    SetChecked(!checked_);
  }
}

}  // namespace ptk
