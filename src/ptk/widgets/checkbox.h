
#ifndef SRC_PTK_WIDGETS_CHECKBOX_H_
#define SRC_PTK_WIDGETS_CHECKBOX_H_

#include <functional>
#include <string>

#include "src/ptk/widgets/widget.h"

namespace ptk {

class Checkbox : public Widget {
 public:
  Checkbox(Widget* parent, const char* label, bool initial_checked = false);
  ~Checkbox() override = default;

  bool checked() const { return checked_; }

  void SetChecked(bool checked);

  void SetOnCheckedChangedHandler(std::function<void(bool)>&& handler);

 protected:
  // ptk::Widget:
  DrawState CalculateDrawState() const override;
  void DrawInternal(NVGcontext* nvg_context) override;
  void OnMouseEventInternal(MouseEvent* mouse_event) override;

  std::string label_;

 private:
  std::function<void(bool)> on_checked_changed_;
  bool checked_;

  DISALLOW_COPY_AND_ASSIGN(Checkbox);
};

}  // namespace ptk

#endif  // SRC_PTK_WIDGETS_CHECKBOX_H_
