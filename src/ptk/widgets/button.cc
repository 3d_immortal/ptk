
#include "src/ptk/widgets/button.h"

#include "src/ptk/events/event.h"
#include "src/third_party/blendish/blendish.h"

namespace ptk {

Button::Button(Widget* parent, const char* label, int icon_id)
    : Label(parent, label, icon_id) {}

void Button::SetOnMouseEventHandler(
    std::function<bool(MouseEvent*)>&& handler) {
  on_mouse_event_ = std::move(handler);
}

ptk::Widget::DrawState Button::CalculateDrawState() const {
  if (has_focus() && has_hover())
    return DrawState::ACTIVE;

  if (has_hover())
    return DrawState::HOVER;

  return DrawState::NORMAL;
}

void Button::DrawInternal(NVGcontext* nvg_context) {
  bndToolButton(nvg_context, bounds_.x_, bounds_.y_, bounds_.width_,
                bounds_.height_, BND_CORNER_NONE,
                static_cast<BNDwidgetState>(draw_state_), icon_id_,
                label_.c_str());
}

void Button::OnMouseEventInternal(MouseEvent* mouse_event) {
  set_has_focus(mouse_event->flags_ &
                (Event::FLAG_PRESSED | Event::FLAG_REPEAT));
  if (mouse_event->GetType() == Event::Type::kMouseCapture)
    return;

  if (on_mouse_event_ && on_mouse_event_(mouse_event))
    mouse_event->handled_ = true;
}

}  // namespace ptk
