
#include "src/ptk/widgets/root_widget.h"

#include "src/third_party/blendish/blendish.h"

namespace ptk {

RootWidget::RootWidget(const Rect& bounds) : Widget(nullptr, bounds) {}

void RootWidget::DrawInternal(NVGcontext* nvg_context) {
  bndBackground(nvg_context, 0, 0, bounds_.width_, bounds_.height_);
  bndBevel(nvg_context, 0, 0, bounds_.width_, bounds_.height_);
}

}  // namespace ptk
