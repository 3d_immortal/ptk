
#ifndef SRC_PTK_WIDGETS_ROOT_WIDGET_H_
#define SRC_PTK_WIDGETS_ROOT_WIDGET_H_

#include "src/ptk/widgets/widget.h"

namespace ptk {

class RootWidget : public Widget {
 public:
  RootWidget(const Rect& bounds);
  ~RootWidget() override = default;

 private:
  // ptk::Widget:
  void DrawInternal(NVGcontext* nvg_context) override;

  DISALLOW_COPY_AND_ASSIGN(RootWidget);
};

}  // namespace ptk

#endif  // SRC_PTK_WIDGETS_ROOT_WIDGET_H_
