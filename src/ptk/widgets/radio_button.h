
#ifndef SRC_PTK_WIDGETS_RADIO_BUTTON_H_
#define SRC_PTK_WIDGETS_RADIO_BUTTON_H_

#include "src/ptk/widgets/checkbox.h"

namespace ptk {

class RadioButton : public Checkbox {
 public:
  RadioButton(Widget* parent,
              const char* label,
              int icon_id,
              bool initial_checked = false);
  ~RadioButton() override = default;

 private:
  // ptk::Widget:
  void DrawInternal(NVGcontext* nvg_context) override;

 private:
  int icon_id_;

  DISALLOW_COPY_AND_ASSIGN(RadioButton);
};

}  // namespace ptk

#endif  // SRC_PTK_WIDGETS_RADIO_BUTTON_H_
