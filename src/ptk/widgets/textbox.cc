
#include "src/ptk/widgets/textbox.h"

#include <GLFW/glfw3.h>

#include "src/ptk/events/event.h"
#include "src/third_party/blendish/blendish.h"

namespace ptk {

Textbox::Textbox(Widget* parent, size_t initial_buffer_size) : Widget(parent) {
  text_buffer_.reserve(initial_buffer_size);
}

void Textbox::DrawInternal(NVGcontext* nvg_context) {
  bndTextField(nvg_context, bounds_.x_, bounds_.y_, bounds_.width_,
               bounds_.height_, BND_CORNER_NONE,
               static_cast<BNDwidgetState>(draw_state_), -1,
               text_buffer_.c_str(), text_buffer_.size(), text_buffer_.size());
}

void Textbox::OnMouseEventInternal(MouseEvent* mouse_event) {
  if (mouse_event->flags_ & Event::FLAG_PRESSED)
    set_has_focus(true);
}

void Textbox::OnKeyEventInternal(KeyEvent* key_event) {
  if (key_event->flags_ & Event::FLAG_RELEASED)
    return;

  if (key_event->key_ == GLFW_KEY_BACKSPACE && !text_buffer_.empty())
    text_buffer_.erase(text_buffer_.size() - 1);

  if (key_event->key_ == GLFW_KEY_ENTER)
    text_buffer_.push_back('\n');
}

void Textbox::OnCharEventInternal(CharEvent* char_event) {
  // TODO: Move this check to the CharEvent constructor.
  if (char_event->key_ >= 32 && char_event->key_ <= 255)
    text_buffer_.push_back((char)(char_event->key_));
}

}  // namespace ptk
