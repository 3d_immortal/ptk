
#include "src/ptk/widgets/slider.h"

#include "src/ptk/events/event.h"
#include "src/third_party/blendish/blendish.h"

namespace ptk {

Slider::Slider(Widget* parent, const char* label) : Label(parent, label, -1) {}

void Slider::DrawInternal(NVGcontext* nvg_context) {
  static char value[32];
  sprintf(value, "%.0f%%", progress_ * 100.0f);
  bndSlider(nvg_context, bounds_.x_, bounds_.y_, bounds_.width_,
            bounds_.height_, BND_CORNER_NONE,
            static_cast<BNDwidgetState>(draw_state_), progress_, label_.c_str(),
            value);
}

void Slider::OnMouseEventInternal(MouseEvent* mouse_event) {
  if (mouse_event->GetType() != Event::Type::kMouseCapture) {
    if (mouse_event->flags_ & Event::FLAG_PRESSED)
      start_progress_ = progress_;
    return;
  }

  MouseCapture* capture = static_cast<MouseCapture*>(mouse_event);
  const double delta_x = capture->x_ - capture->start_x_;
  progress_ = start_progress_ + (delta_x / bounds_.width_);
  progress_ = (progress_ < 0) ? 0 : (progress_ > 1.0f) ? 1.0f : progress_;
}

}  // namespace ptk
