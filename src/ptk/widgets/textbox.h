
#ifndef SRC_PTK_WIDGETS_TEXTBOX_H_
#define SRC_PTK_WIDGETS_TEXTBOX_H_

#include "src/ptk/widgets/widget.h"

namespace ptk {

class Textbox : public Widget {
 public:
  Textbox(Widget* parent, size_t initial_buffer_size = 1024);
  ~Textbox() override = default;

 private:
  // ptk::Widget:
  void DrawInternal(NVGcontext* nvg_context) override;
  void OnMouseEventInternal(MouseEvent* mouse_event) override;
  void OnKeyEventInternal(KeyEvent* key_event) override;
  void OnCharEventInternal(CharEvent* char_event) override;

 private:
  std::string text_buffer_;

  DISALLOW_COPY_AND_ASSIGN(Textbox);
};

}  // namespace ptk

#endif  // SRC_PTK_WIDGETS_TEXTBOX_H_
