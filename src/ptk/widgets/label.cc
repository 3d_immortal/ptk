
#include "src/ptk/widgets/label.h"

#include "src/third_party/blendish/blendish.h"

namespace ptk {

Label::Label(Widget* parent, const char* label, int icon_id)
    : Widget(parent), label_(label), icon_id_(icon_id) {}

void Label::DrawInternal(NVGcontext* nvg_context) {
  bndLabel(nvg_context, bounds_.x_, bounds_.y_, bounds_.width_, bounds_.height_,
           icon_id_, label_.c_str());
}

}  // namespace ptk
