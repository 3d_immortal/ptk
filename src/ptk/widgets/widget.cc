
#include "src/ptk/widgets/widget.h"

#include <cmath>

#include "src/ptk/events/event.h"
#include "src/ptk/layout/layout_strategy.h"
#include "src/third_party/nanovg/nanovg/src/nanovg.h"

namespace ptk {

Widget::Widget(Widget* parent, const Rect& bounds) : bounds_(bounds) {
  // TODO: Add memory management.
  if (parent)
    parent->AddChild(this);
}

Widget::~Widget() {}

// static
Widget* Widget::focused_widget_ = nullptr;

void Widget::AddChild(Widget* child) {
  DCHECK(child);
  DCHECK(!child->added_as_a_child_);
  children_.emplace_back(child);
  child->added_as_a_child_ = true;
}

void Widget::SetBounds(const Rect& bounds) {
  bounds_ = bounds;
  if (layout_strategy_)
    layout_strategy_->Layout(this);
}

void Widget::Draw(NVGcontext* nvg_context) {
  draw_state_ = CalculateDrawState();

  // Draw self, then children.
  nvgGlobalAlpha(nvg_context, is_enabled_ ? 1.0f : 0.5f);
  DrawInternal(nvg_context);
  for (auto* child : children_)
    child->Draw(nvg_context);
  nvgGlobalAlpha(nvg_context, 1.0f);
  draw_state_ = DrawState::NORMAL;
}

Widget* Widget::FindHoverWidget(double x, double y) {
  if (!is_enabled_)
    return nullptr;
  if (!bounds_.Intersects(std::floor(x), std::floor(y)))
    return nullptr;
  for (auto* child : children_) {
    Widget* hover_child = child->FindHoverWidget(x, y);
    if (hover_child)
      return hover_child;
  }
  return this;
}

void Widget::OnMouseEvent(MouseEvent* mouse_event) {
  DCHECK(!mouse_event->handled_);
  DCHECK(is_enabled_);
  DCHECK(bounds_.Intersects(std::floor(mouse_event->x_),
                            std::floor(mouse_event->y_)));

  OnMouseEventInternal(mouse_event);
}

void Widget::OnKeyEvent(KeyEvent* key_event) {
  DCHECK(is_enabled_);
  DCHECK(has_focus_);
  DCHECK(Widget::focused_widget_ == this);

  OnKeyEventInternal(key_event);
}

void Widget::OnCharEvent(CharEvent* char_event) {
  DCHECK(is_enabled_);
  DCHECK(has_focus_);
  DCHECK(Widget::focused_widget_ == this);

  OnCharEventInternal(char_event);
}

Widget::DrawState Widget::CalculateDrawState() const {
  if (has_focus_)
    return DrawState::ACTIVE;

  if (has_hover_)
    return DrawState::HOVER;

  return DrawState::NORMAL;
}

void Widget::DrawInternal(NVGcontext* nvg_context) {}

void Widget::OnMouseEventInternal(MouseEvent* mouse_event) {}

void Widget::OnKeyEventInternal(KeyEvent* key_event) {}

void Widget::OnCharEventInternal(CharEvent* char_event) {}

}  // namespace ptk
