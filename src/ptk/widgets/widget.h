
#ifndef SRC_PTK_WIDGETS_WIDGET_H_
#define SRC_PTK_WIDGETS_WIDGET_H_

#include <memory>
#include <vector>

#include "core/src/macros.h"
#include "src/ptk/layout/rect.h"
#include "src/third_party/blendish/blendish.h"

struct NVGcontext;

namespace ptk {

class CharEvent;
class KeyEvent;
class LayoutStrategy;
class MouseEvent;

class Widget {
 public:
  enum class DrawState {
    NORMAL = BND_DEFAULT,
    HOVER = BND_HOVER,
    ACTIVE = BND_ACTIVE,
  };

  Widget(Widget* parent, const Rect& bounds = {});
  virtual ~Widget();

  static Widget* focused_widget_;

  void AddChild(Widget* child);

  template <typename T, typename... Args>
  void SetLayoutStrategy(Args&&... args) {
    static_assert(std::is_base_of<LayoutStrategy, T>::value,
                  "`T` must be a sub-class of `LayoutStrategy`");
    layout_strategy_ = std::make_unique<T>(std::forward<Args>(args)...);
  }

  void SetBounds(const Rect& bounds);

  void Draw(NVGcontext* nvg_context);

  Widget* FindHoverWidget(double x, double y);

  void OnMouseEvent(MouseEvent* mouse_event);

  void OnKeyEvent(KeyEvent* key_event);

  void OnCharEvent(CharEvent* char_event);

  bool is_enabled() const { return is_enabled_; }
  void set_is_enabled(bool value) {
    // TODO: See if you need to propagate to children.
    is_enabled_ = value;
  }

  bool has_focus() const { return has_focus_; }
  void set_has_focus(bool value) {
    // TODO: Change to Focus() / Blur().
    has_focus_ = value;

    if (value) {
      if (Widget::focused_widget_ != this) {
        if (Widget::focused_widget_)
          Widget::focused_widget_->has_focus_ = false;

        Widget::focused_widget_ = this;
      }
    } else {
      if (Widget::focused_widget_ == this)
        Widget::focused_widget_ = nullptr;
    }
  }

  bool has_hover() const { return has_hover_; }
  void set_has_hover(bool value) { has_hover_ = value; }

  const Rect& bounds() const { return bounds_; }
  const std::vector<Widget*>& children() const { return children_; }

 protected:
  Rect bounds_;

  DrawState draw_state_{DrawState::NORMAL};

 private:
  friend class Application;

  virtual DrawState CalculateDrawState() const;

  virtual void DrawInternal(NVGcontext* nvg_context);

  virtual void OnMouseEventInternal(MouseEvent* mouse_event);

  virtual void OnKeyEventInternal(KeyEvent* key_event);

  virtual void OnCharEventInternal(CharEvent* char_event);

  std::vector<Widget*> children_;
  std::unique_ptr<LayoutStrategy> layout_strategy_;

  bool added_as_a_child_{false};
  bool is_enabled_{true};
  bool has_focus_{false};
  bool has_hover_{false};

  DISALLOW_COPY_AND_ASSIGN(Widget);
};

}  // namespace ptk

#endif  // SRC_PTK_WIDGETS_WIDGET_H_
