
#ifndef SRC_PTK_WIDGETS_SLIDER_H_
#define SRC_PTK_WIDGETS_SLIDER_H_

#include "src/ptk/widgets/label.h"

namespace ptk {

class Slider : public Label {
 public:
  Slider(Widget* parent, const char* label);
  ~Slider() override = default;

  float progress() const { return progress_; }

 private:
  // ptk::Widget:
  void DrawInternal(NVGcontext* nvg_context) override;
  void OnMouseEventInternal(MouseEvent* mouse_event) override;

 private:
  float start_progress_{0.0f};
  float progress_{0.0f};

  DISALLOW_COPY_AND_ASSIGN(Slider);
};

}  // namespace ptk

#endif  // SRC_PTK_WIDGETS_SLIDER_H_
