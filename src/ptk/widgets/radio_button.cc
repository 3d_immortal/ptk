
#include "src/ptk/widgets/radio_button.h"

#include "src/third_party/blendish/blendish.h"

namespace ptk {

RadioButton::RadioButton(Widget* parent,
                         const char* label,
                         int icon_id,
                         bool initial_checked)
    : Checkbox(parent, label, initial_checked), icon_id_(icon_id) {}

void RadioButton::DrawInternal(NVGcontext* nvg_context) {
  bndRadioButton(nvg_context, bounds_.x_, bounds_.y_, bounds_.width_,
                 bounds_.height_, BND_CORNER_NONE,
                 static_cast<BNDwidgetState>(draw_state_), icon_id_,
                 label_.c_str());
}

}  // namespace ptk
