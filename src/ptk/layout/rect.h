
#ifndef SRC_PTK_LAYOUT_RECT_H_
#define SRC_PTK_LAYOUT_RECT_H_

#include <string>

namespace ptk {

struct Rect {
  int x_{0};
  int y_{0};
  int width_{0};
  int height_{0};

  bool Intersects(int x, int y) const;

  std::string ToString() const;
};

}  // namespace ptk

#endif  // SRC_PTK_LAYOUT_RECT_H_
