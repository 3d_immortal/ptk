
#ifndef SRC_PTK_LAYOUT_LAYOUT_STRATEGY_H_
#define SRC_PTK_LAYOUT_LAYOUT_STRATEGY_H_

#include "core/src/macros.h"

namespace ptk {

class Widget;

class LayoutStrategy {
 public:
  LayoutStrategy() {}
  virtual ~LayoutStrategy() {}

  // Lays out the children of |widget|.
  virtual void Layout(Widget* widget) = 0;

 private:
  DISALLOW_COPY_AND_ASSIGN(LayoutStrategy);
};

}  // namespace ptk

#endif  // SRC_PTK_LAYOUT_LAYOUT_STRATEGY_H_
