
#ifndef SRC_PTK_LAYOUT_FILL_LAYOUT_H_
#define SRC_PTK_LAYOUT_FILL_LAYOUT_H_

#include "src/ptk/layout/layout_strategy.h"

namespace ptk {

class FillLayout : public LayoutStrategy {
 public:
  enum class Type {
    kVertical,
    kHorizontal,
  };

  FillLayout(Type type = Type::kVertical,
             int vertical_margin = 2,
             int horizontal_margin = 2,
             int spacing = 2);
  ~FillLayout() override = default;

  // LayoutStrategy:
  void Layout(Widget* widget) override;

 private:
  void LayoutVertical(Widget* widget) const;

  void LayoutHorizontal(Widget* widget) const;

 private:
  const Type type_;
  const int vertical_margin_;
  const int horizontal_margin_;
  const int spacing_;

  DISALLOW_COPY_AND_ASSIGN(FillLayout);
};

}  // namespace ptk

#endif  // SRC_PTK_LAYOUT_FILL_LAYOUT_H_
