
#include "src/ptk/layout/rect.h"

#include <sstream>

namespace ptk {

bool Rect::Intersects(int x, int y) const {
  return !(x <= x_ || x >= (x_ + width_) || y <= y_ || y >= (y_ + height_));
}

std::string Rect::ToString() const {
  std::stringstream ss;
  ss << "x = " << x_ << ", y = " << y_ << ", width = " << width_
     << ", height = " << height_;
  return ss.str();
}

}  // namespace ptk
