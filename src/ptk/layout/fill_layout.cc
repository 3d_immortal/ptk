
#include "src/ptk/layout/fill_layout.h"

#include "src/ptk/widgets/widget.h"

namespace ptk {

FillLayout::FillLayout(Type type,
                       int vertical_margin,
                       int horizontal_margin,
                       int spacing)
    : type_(type),
      vertical_margin_(vertical_margin),
      horizontal_margin_(horizontal_margin),
      spacing_(spacing) {}

void FillLayout::Layout(Widget* widget) {
  if (type_ == Type::kVertical)
    LayoutVertical(widget);
  else
    LayoutHorizontal(widget);
}

void FillLayout::LayoutVertical(Widget* widget) const {
  const Rect& parent_bounds = widget->bounds();
  const auto& children = widget->children();
  const auto num_children = children.size();
  const int child_height = (parent_bounds.height_ - (2 * vertical_margin_) -
                            ((num_children - 1) * spacing_)) /
                           num_children;
  const int child_width = parent_bounds.width_ - (2 * horizontal_margin_);
  const int x = parent_bounds.x_ + horizontal_margin_;
  int y = parent_bounds.y_ + vertical_margin_;
  for (auto* child : children) {
    child->SetBounds({x, y, child_width, child_height});
    y += child_height + spacing_;
  }
}

void FillLayout::LayoutHorizontal(Widget* widget) const {
  const Rect& parent_bounds = widget->bounds();
  const auto& children = widget->children();
  const auto num_children = children.size();
  const int child_height = parent_bounds.height_ - (2 * vertical_margin_);
  const int child_width = (parent_bounds.width_ - (2 * horizontal_margin_) -
                           ((num_children - 1) * spacing_)) /
                          num_children;
  int x = parent_bounds.x_ + horizontal_margin_;
  const int y = parent_bounds.y_ + vertical_margin_;
  for (auto* child : children) {
    child->SetBounds({x, y, child_width, child_height});
    x += child_width + spacing_;
  }
}

}  // namespace ptk
