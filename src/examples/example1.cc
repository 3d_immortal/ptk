
#include <iostream>

#include "src/ptk/application/application.h"
#include "src/ptk/events/event.h"
#include "src/ptk/layout/fill_layout.h"
#include "src/ptk/widgets/button.h"
#include "src/ptk/widgets/checkbox.h"
#include "src/ptk/widgets/label.h"
#include "src/ptk/widgets/radio_button.h"
#include "src/ptk/widgets/root_widget.h"
#include "src/ptk/widgets/slider.h"
#include "src/ptk/widgets/textbox.h"
#include "src/ptk/widgets/widget.h"

#define OUT(stream) std::cout << stream << std::endl;

int main() {
  ptk::Application app{2.0f};
  if (!app.Init())
    return -1;

  // Use a fill layout.
  app.root_widget()->SetLayoutStrategy<ptk::FillLayout>(
      ptk::FillLayout::Type::kHorizontal);

  ptk::Widget left{app.root_widget()};
  left.SetLayoutStrategy<ptk::FillLayout>();
  ptk::Button b1{&left, "Test 1", BND_ICON_SAVE_PREFS};
  ptk::Button b2{&left, "Test 2", BND_ICON_REC};
  ptk::Button b3{&left, "Test 3", BND_ICON_PLAY};

  b1.SetOnMouseEventHandler([](ptk::MouseEvent* me) -> bool {
    if (me->flags_ & ptk::Event::FLAG_PRESSED) {
      OUT("Received mouse click at [" << me->x_ << ", " << me->y_ << "].");
      return true;
    }
    return false;
  });

  ptk::Widget right{app.root_widget()};
  right.SetLayoutStrategy<ptk::FillLayout>();
  ptk::Label label{&right, "Label 1", BND_ICON_FILE};
  ptk::Slider slider{&right, "Slider"};
  ptk::Checkbox check1{&right, "Easy?", true};
  ptk::Checkbox check2{&right, "Disable radios", false};
  ptk::RadioButton radio1{&right, "Radio 1", BND_ICON_SOUND, false};
  ptk::RadioButton radio2{&right, "Radio 2", BND_ICON_ACTION, true};
  check2.SetOnCheckedChangedHandler([&](bool checked) {
    radio1.set_is_enabled(!checked);
    radio2.set_is_enabled(!checked);
  });
  ptk::Textbox textbox{&right};
  ptk::Textbox textbox2{&right};

  return app.Run();
}
